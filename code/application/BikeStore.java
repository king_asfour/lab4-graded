//Name: Samir Abo-Assfour
//stud. id: 2137408
package application;

import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] myBike = new Bicycle[4];

        myBike[0] = new Bicycle("Speciliazed", 21,40.2);
        myBike[1] = new Bicycle("TLabs bike", 31, 60);
        myBike[2] = new Bicycle("Cycling Avenue", 18, 34);
        myBike[3] = new Bicycle("Canadian MT", 25, 51.6);

        for(int i = 0; i < myBike.length; i++){
            System.out.println("This is the bike #" + (i + 1) + ": " + myBike[i]);
            System.out.println(" ");
        }
    }
}
